# Mail It! #

A chrome extension that helps you mail to multiple people by getting their email addresses from a single page.

I came up with this idea when I was contacting universities during my undergraduate year. Just install the extension into your Chrome browser and the extension automatically retrieves all the email ids from any page, composes a mail with a customized text and redirects to your gmail page where you just have to click send - As simple as that!

Feel free to contact me if you have any issues regarding the same!