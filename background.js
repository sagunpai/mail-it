/**
 * Handles all message communication for the for the background page for the extension.
 */

var baseGmailUrl = "https://mail.google.com/";
var gmailUrlSuffix = "mail/?view=cm&fs=1&tf=1";

function makeGmailDomainUrl() {
  var gmailUrl = baseGmailUrl;
  var domainName = window.localStorage["domainName"];
  if (domainName) {
    gmailUrl += "a/" + domainName + "/";
  }
  return gmailUrl + gmailUrlSuffix;
}

var subjectPrefix = '';
var title = '';
var url = '';
// selected text from the current tab passed from content script.
var selectedText = '';

chrome.extension.onConnect.addListener(
  function(port) {
    if (port.name == "GmailUrlConn") {
      port.onMessage.addListener(function(msg) {
      if (msg.req == "GmailUrlPlease") {
        port.postMessage({gmailDomainUrl: makeGmailDomainUrl()});
      } else {
        console.log("Unsupported request!");
      }
    });
  }
});

chrome.extension.onRequest.addListener(
  function(connectionInfo) {
    selectedText = connectionInfo;
    makeGmailWin(selectedText);
});

chrome.browserAction.onClicked.addListener(
  function(tab) {
    chrome.tabs.executeScript(null, {file: "infopasser.js"});
    title = tab.title;
    url = tab.url;
});

function getMails ( text ){
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    }

function makeGmailWin(summary) {
  // Ensure this is the active window
  var body = '';
  console.log("Summary = " + summary);
  var subject = "";
  if (localStorage["subjectPrefix"]) {
    subject += localStorage["subjectPrefix"] + " - ";
  }
  subject += title;
  if (summary == '') {
    body = url;
  } else {
    body = summary + "\n" + url;
  }
  
  var mailstring = body + "Modified mail content";
  var mailsubject = "Modified mail subject"
  var htmlstring = $('html').html();
  var mailsenders = getMails(htmlstring).join(','));

  var gmailURL = makeGmailDomainUrl() +
                 "&to" + encodeURIComponent(mailsenders) +
                 "&su=" + encodeURIComponent(mailsubject) +
                 "&body=" + encodeURIComponent(mailstring);
  chrome.windows.create({
    url: gmailURL,
    left: 20,
    top: 30,
    width: 700,
    height: 600
    });
}
