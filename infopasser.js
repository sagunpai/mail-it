/**
 * Passes the selected text of the current tab to the background page.
 */

summaryText();

function summaryText() {
  console.log("Issue Request to background.");
  chrome.extension.sendRequest(window.getSelection().toString());
}
